//
//  AppDelegate.swift
//  D3B3329132D0496790CBD72A3C1049C2
//
//  Created by Kieran Senior on 11/1/17.
//  Copyright © 2017 Apple. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var window: NSWindow!


    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

